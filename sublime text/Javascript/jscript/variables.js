function CrearVariables() {
	// Manejo de la varibale precio.
	var precio = 'a',
		resultado;

		//Sacara la variable precio, si arriba no hubiese 'a', sacaria undefined
		resultado = document.getElementById('resultado');
		resultado.innerHTML +=
			"<br> La varibale precio vale: " +
				precio + "<br>";

		// Aqui la variable precio cambia a ser un numero
		precio = 4;
		precio = precio + 10;
		resultado.innerHTML +=
			"La varibale precio ahora vale: " +
				precio + "<br>" +
			"El doble de precio es: " +
				(precio * 2) + "<br>";

		// Aqui la variable precio cambia a ser un numero a una cadena de caracteres
		precio = "muy caro";
		resultado.innerHTML +=
			"La varibale precio ahora vale: " +
				precio + "<br>" +
			"El doble de precio es: " +
				(precio * 2) + "<br>";
}