var v1,
	v2,
	resultado;

function SumaEnteros() {
	// Suma de enteros
	var v1 = 2;
	var v2 = 4;
	resultado = document.getElementById('resultado');
	resultado.innerHTML +=
			"La varibale " +
				v1 + " + " + "la variable " + v2 +
			" = " + (v1 + v2) + "<br>";

}

function SumaEnteroCadena() {
	// Suma de un entero con cadena
	var v1 = 2;
	var v2 = "adios";
	resultado = document.getElementById('resultado');
	resultado.innerHTML +=
			"La varibale " +
				v1 + " + " + "la variable " + v2 +
			" = " + (v1 + " " + v2) + "<br>";

}

function SumaCadenaEntero() {
	// SUma de cadena con entero
	var v1 = "hola";
	var v2 = 4;
	resultado = document.getElementById('resultado');
	resultado.innerHTML +=
			"La varibale " +
				v1 + " + " + "la variable " + v2 +
			" = " + (v1 + " " + v2) + "<br>";
}

function SumaCadenas() {
	// Suma de cadenas
	var v1 = "hola";
	var v2 = "adios";
	resultado = document.getElementById('resultado');
	resultado.innerHTML +=
			"La varibale " +
				v1 + " + " + "la variable " + v2 +
			" = " + (v1 + " " + v2) + "<br>";
}