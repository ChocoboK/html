function DiaSig2() {
	var dia, mes, anio, resultado;
	dia = parseInt(document.getElementById('dia').value);
	mes = parseInt(document.getElementById('mes').value);
	anio = parseInt(document.getElementById('anio').value);
	resultado = document.getElementById('resultado');
	
	switch(mes){
		case 1: 
		case 3:
		case 5: 
		case 7:
		case 8:
		case 10: if (dia==31) 
					{
						dia=1;
						mes++;

					} else
						dia++;
					  break;
		case 12: if (dia==31) 
					{
						dia=1;
						mes=1;
						anio++;

					} else
						dia++;
					  break;
		case 4:
		case 6:
		case 9:
		case 11: if (dia==30) 
					{
						dia=1;
						mes++;

					} else
						dia++;
					  break;
		case 2: if (((dia==28) && (anio%4!=0)) || ((dia==29) && (anio%4==0))) 
					{
						dia=1;
						mes++;

					} else
						dia++;
					  break;
	}
	resultado.innerHTML +=
		"El dia siguiente es " + dia + "/" + mes + "/" + anio + "<br>";
}