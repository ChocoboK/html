
function CalcularDiaMes() {

	var mesCadena, mes, diasMes, resultado;

	mesCadena = document.getElementById('numeroMes').value;
	mes = parseInt(mesCadena);

	switch(mes){
		case 1: 
		case 3:
		case 5: 
		case 7:
		case 9:
		case 10:
		case 12: diasMes = 31;
			break;

		case 4:
		case 6:
		case 9:
		case 11: diasMes = 31;
			break;
		case 2: diasMes = 28;
			break;
		default: diasMes = 99;
			break;

	}
	resultado = document.getElementById('resultado');
	if (diasMes != 99)
		resultado.innerHTML +=
			"<br> El mes " + mes + " tiene " + diasMes + " dias.";
	else
		resultado.innerHTML +=
			"<br> El mes " + mes + ", no existe... (Prueba del 1 al 12)";
}