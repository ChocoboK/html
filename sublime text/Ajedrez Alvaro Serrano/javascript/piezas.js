/* Esta funcion pondra nuestras piezas de la carpeta imagenes en el menu de las piezas */
function piezasMenu() {
	menu = document.getElementById('piezas');

	for (var i = 0; i < piezas.length; i++) /* Con un for recorremos todo el directorio imagenes*/
		menu.innerHTML += '<img src="imagenes/'+ piezas[i] +'.png" onclick="eligePieza(' + i + ')">';
}

/* Esta funcion guarda la pieza elegida, para ponerla mas tarde (funcion de abajo) */
function eligePieza (p) {
	nombre = piezas[p];
	document.getElementsByClassName('pieza')[0].innerHTML = nombre /* Esto mostrara el nombre de la pieza elegida */
}

/* Esta funcion pondra la pieza en la casilla que queramos */
function ponPieza () {
	if ( fila != undefined && columna != undefined && nombre != undefined) {
		document.getElementsByClassName("f" + fila + " c"+ columna)[0].innerHTML = "<img src='imagenes/" + nombre + ".png'> ";
	}
}