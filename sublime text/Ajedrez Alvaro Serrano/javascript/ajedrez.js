/* Array con las piezas que piden */
var piezas = ["Alfil", "Torre", "Rey", "Reina", "Peon"];
var nombre
var numumero
var tamanio = 8

/* Esta funcion creara el tablero */
function Tablero () {
	var tablero = document.getElementById('tablero');
	var filas;
	
	/* Primero crearemos las filas del tablero con un div */
	for(var f = 0; f < tamanio; f++) {
		tablero.innerHTML += "<div class=\"fila f" + f + "\">";
	}

	/* Una vez creada las filas, crearemos las columnas con div y su respectivo color */
	for(var f = 0; f < tamanio; f++) {
		filas  = document.getElementsByClassName('fila')[f];
		for (var c = 0; c < tamanio; c++) {
			if ( f % 2 == ( c % 2) ) // div blancos donde no seas divisible entre 2  y los div negros en las que lo son
				filas.innerHTML += "<div class='columna f" + f + " c" + c +"'style='background-color: rgb(180,180,180);'" +
								"onclick='guardaPosi("+ f +", "+ c +"), ponerPosi(), pieza(this)'></div>"; /* Esta parte del div cojera la posicion del div y podra poner la ficha que queramos */
			else
				filas.innerHTML += "<div class='columna f" + f + " c" + c +"'style='background-color: rgb(30,30,30);'" +
								"onclick='guardaPosi("+ f +", "+ c +"), ponerPosi(), pieza(this)'></div>"; /* Esto hara lo mismo que explique arriba */
		}
	}
}

/* Esto mostara la posicion seleccionada, cogiendo la fila y la columna */
function guardaPosi(f, c) {
	fila = f;
	columna = c;
}

/* Esta otra funcion te coge los parametros de arriba para mostrartelos en el menu */
function ponerPosi() {
	fila = fila 
	columna = columna
	document.getElementsByClassName('posicion')[0].innerHTML = fila;
	document.getElementsByClassName('posicion')[1].innerHTML = columna;
}